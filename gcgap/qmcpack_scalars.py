import numpy as np
def get_en_mats(fjson):
  import pandas as pd
  df = pd.read_json(fjson)
  dsel = np.ones(len(df), dtype=bool)
  df = df.loc[dsel].copy()
  df.sort_values(['nelec', 'group'], inplace=True)
  myx = df['nelec'].unique()
  ntwist = len(df['group'].unique())

  # emat: energy of each state
  ematm = df['LocalEnergy_mean'].values.reshape(len(myx), ntwist)
  emate = df['LocalEnergy_error'].values.reshape(len(myx), ntwist)

  # nmat: particle number of each state
  nmat = np.zeros(ematm.shape, dtype=int)
  for irow in range(len(nmat)):
    nmat[irow, :] = myx[irow]
  return ematm, emate, nmat

def find_vmax_cmin(murm, mure, muam, muae):
  iv = np.argmax(murm)
  vmaxm = murm[iv]; vmaxe = mure[iv]
  ic = np.argmin(muam)
  cminm = muam[ic]; cmine = muae[ic]
  entry = {'ic': ic, 'cmin_mean': cminm, 'cmin_error': cmine,
           'iv': iv, 'vmax_mean': vmaxm, 'vmax_error': vmaxe}
  return entry
