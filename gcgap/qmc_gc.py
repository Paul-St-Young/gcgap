import numpy as np

def get_dmat(ematm, emate):
  # dmat: energy difference
  dmatm = (ematm[1:, :] - ematm[:-1, :])/2.
  dmate = 0.5*(emate[1:, :]**2 + emate[:-1, :]**2)**0.5
  return dmatm, dmate

def get_add_remove(dmatm, dmate, ymult):
  #yunit = 'eV'
  #ymult = 27.2114
  eaddm = dmatm[1:]*ymult
  eadde = dmate[1:]*ymult
  edelm = dmatm[:-1]*ymult
  edele = dmate[:-1]*ymult
  return eaddm, eadde, edelm, edele

def build_omat(mymu, dmatm):
  """ construct occupation matrix for many-body states at all twist

  Args:
    mymu (np.array): a list of chemical potentials
    dmatm (np.array): a matrix of total energy differences
  Return:
    omat (np.array): many-body state occupation matrix
  """
  nstatem1, ntwist = dmatm.shape
  omat = np.zeros([len(mymu), nstatem1-1, ntwist], dtype=bool)

  for imu, mu in enumerate(mymu):
    sel1 = dmatm>mu
    sel2 = dmatm<=mu
    sel = sel1[1:] & sel2[:-1]
    omat[imu][sel] = True
  #filter_omat(omat)
  return omat

def save_omat(mym, omat):
  import tables
  from qharv.reel.config_h5 import save_mat, save_vec
  filters = tables.Filters(complevel=5, complib='zlib')
  fp = tables.open_file('omat.h5', mode='w', filters=filters)
  slab = fp.create_group('/', 'omat')
  save_mat(omat, fp, slab, 'data')
  slab = fp.create_group('/', 'mu')
  save_vec(mymu, fp, slab, 'data')
  fp.close()

def gather_band(fjson, ymult):
  import pandas as pd
  from gcgap.qmcpack_scalars import get_en_mats
  ematm, emate, nmat = get_en_mats(fjson)
  dmatm, dmate = get_dmat(ematm, emate)
  eaddm, eadde, ermm, erme = get_add_remove(dmatm, dmate, ymult)

  # get mua mur df
  nstate, ntwist = eaddm.shape
  imid = (nstate-1)/2
  entryl = []
  for itwist in range(ntwist):
    entry = {
      'group': itwist,
      'mua_mean': eaddm[imid, itwist],
      'mua_error': eadde[imid, itwist],
      'mur_mean': ermm[imid, itwist],
      'mur_error': erme[imid, itwist],
    }
    entryl.append(entry)
  df = pd.DataFrame(entryl)
  return df

def get_band_edge(bdf, valence=False, prefix=''):
  if valence:
    yname = prefix+'mua'
    cfunc = np.argmin
  else:
    yname = prefix+'mur'
    cfunc = np.argmax
  x = bdf['group'].values
  ym = bdf['%s_mean' % yname].values
  ye = bdf['%s_error' % yname].values
  idx = cfunc(ym)
  return x[idx], ym[idx], ye[idx]

def gcen(mu, mur, mua, weights=None, ntot=None):
  """Calculate GC twist-averaged energy E and number of electrons N.

  Args:
    mu (float): chemical potential
    mur (np.array): shape (ntwist,) energy to remove an electron at each twist
    mua (np.array): shape (ntwist,) energy to add an electron at each twist
    weights (np.array, optional): weights of twists, default is None
    ntot (int, optional): total number of twists (if spin *2), default is None
  Return:
    (float, float): GCTA (energy E, number N)
  """
  if len(mur) != len(mua):
    raise RuntimeError('%d removal != %d addition energies' %
                       (len(mur), len(mua)))
  if weights is None:
    weights = 2*np.ones(len(mur))  # !!!! assume spin degeneracy
  elif ntot is None:
    raise RuntimeError('must provide the total # of twists with weights')
  elif not np.isclose(weights.sum(), ntot):
    msg = 'weights do NOT sum to the total number of twists!\n'
    msg += '  the returned averages may not be normalized correctly.\n'
    raise RuntimeError(msg)
  elif len(weights) != len(mur):
    raise RuntimeError()
  rsel = mu < mur
  asel = mu > mua
  energy = np.sum(mua[asel]*weights[asel]) -\
           np.sum(mur[rsel]*weights[rsel])
  nelec = np.sum(weights[asel]) - np.sum(weights[rsel])
  wtot = float(weights.sum())
  return energy/wtot, nelec/wtot

class GrandCanonicalTwistAverageBands:
  def __init__(self, nvec, ematm, emate=None, weights=None, ntot=None,
               ymult=27.2114):
    """Store database of ground-state energies organized by (Nelec, twist)

    !!!! expect energies to be in Hartree atomic units

    Args:
      nvec (np.array): integer array of shape (nstate,), which
        contains the total number of electrons of each state
      ematm (np.array): float array of shape (nstate, ntwist), which
        contains the mean ground-state total energies of each many-body state
      emate (np.array, optional): error array, default None
      weights (np.array, optional): weights of kpoints, default None
      nktot (int, optional): total number of electrons
      ymult (float, optional): energy multiplier, default is 27.2114 for ha->eV
    """
    idx = np.argsort(nvec)  # sort states from fewest to most electrons
    # initialize matrices
    nmat = np.zeros(ematm.shape, dtype=int)
    for irow in range(len(nvec)):
      nmat[irow, :] = nvec[idx][irow]
    self._nmat = nmat
    self._ematm = ematm[idx]*ymult
    if emate is None:  # assign tiny error
      emate = 1e-16*np.ones(ematm.shape, dtype=ematm.dtype)
    self._emate = emate[idx]*ymult
    # initialize weights
    ntwist = ematm.shape[1]
    self._ntwist = ntwist
    if weights is None:  # !!!! assume spin degeneracy
      weights = 2*np.ones(ntwist)
      ntot = weights.sum()
    else:
      raise NotImplementedError('folded kgrid is NOT debugged yet')
      wtot = weights.sum()
      if not np.isclose(wtot, ntot):
        raise RuntimeError('total weight %3.2f != Nelec %d'  % (wtot, ntot))
    self._weights = weights
    # flags
    self._has_mur_mua = False
  def get_bands(self):
    """Calculate energy differences between states.
    These are the equivalent of bands in an effetive single-particle theory.

    Return:
      (np.array, np.array): (dmatm, dmate), mean and error of bands
    """
    dmatm = 0.5*(self._ematm[1:, :] - self._ematm[:-1, :])
    dmate = 0.5*(self._emate[1:, :]**2 + self._emate[:-1, :]**2)**0.5
    return dmatm, dmate
  def get_mur_mua(self):
    """Extract energies needed to remove(r) or add(a) an electron given bands.

    Args:
    Return:
      (np.array,)*4: (murm, mure, muam, muae), mean and error of remove and add
    """
    dmatm, dmate = self.get_bands()
    nband = len(dmatm)
    if nband != 2:
      raise RuntimeError('%d bands found; not 2' % nband)
    murm = dmatm[0]
    mure = dmate[0]
    muam = dmatm[1]
    muae = dmate[1]
    self._murm = murm
    self._mure = mure
    self._muam = muam
    self._muae = muae
    self._has_mur_mua = True
    return murm, mure, muam, muae
  def set_mur_mua(self, murm, mure, muam, muae):
    self._murm = murm
    self._mure = mure
    self._muam = muam
    self._muae = muae
    self._has_mur_mua = True
  def gcen_mur_mua(self, mu):
    """Calculate GCTA energy E and number of electrons N.
    Using two-band approach, only the two bands cloest to the gap are needed.

    Args:
      mu (float): checmical potential
      mur (np.array): remove energies at each twist
    Return:
      (np.array,)*4: mean and error of E and N
    """
    if not self._has_mur_mua:
      raise RuntimeError('please call get_mur_mua() before gcen')
    murm = self._murm
    mure = self._mure
    muam = self._muam
    muae = self._muae
    # select twists
    rsel = mu < murm  # remove electrons
    asel = mu > muam  # add electrons
    nsel = np.ones(self._ntwist, dtype=bool) & (~rsel) & (~asel)
    ematm = self._ematm
    emate = self._emate
    nmat = self._nmat
    evecm = np.concatenate([
      ematm[0, rsel],
      ematm[1, nsel],
      ematm[2, asel]
    ], axis=0)
    evece = np.concatenate([
      emate[0, rsel],
      emate[1, nsel],
      emate[2, asel]
    ], axis=0)
    nvecm = np.concatenate([
      nmat[0, rsel],
      nmat[1, nsel],
      nmat[2, asel]
    ], axis=0)
    nvece = np.exp(-(mu-muam)**2/(2*muae**2))
    nvece += np.exp(-(mu-murm)**2/(2*mure**2))
    # twist average
    weights = self._weights
    wtot = weights.sum()
    em = np.dot(evecm, weights)/wtot
    ee = np.dot(evece**2, weights)**0.5/wtot
    nm = np.dot(nvecm, weights)/wtot
    ne = np.dot(nvece**2, weights)**0.5/wtot
    return em, ee, nm, ne
# end class GrandCanonicalTwistAverageBands
