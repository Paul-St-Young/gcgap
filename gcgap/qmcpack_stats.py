def get_dsk1d(norb, series, stat_dir, **kwargs):
  """Get fluctuating dS(k) from gathered h5 file."""
  from qharv.reel import mole
  from qharv.sieve.scalar_h5 import twist_average_h5
  import static_correlation as sc
  if 'prefix' in kwargs:
    prefix = kwargs.pop('prefix')
  else:
    prefix = ''
  fregex = '*%s*norb%d*s00%d.dsk.h5' % (prefix, norb, series)
  fh5 = mole.find(fregex, stat_dir)
  # twists are sorted (asserted in twist_average_h5)
  meta, dskm0, dske0 = twist_average_h5(
    fh5, msuffix='m', esuffix='e', **kwargs
  )
  dskm = dskm0.sum(axis=0)
  dske = (dske0**2).sum(axis=0)**0.5
  kvecs = meta['kvecs']
  uk, uskm, uske = sc.shavg(kvecs, dskm, dske)
  return uk, uskm, uske

def get_pure1d(norb, stat_dir, **kwargs):
  """Get ``pure'' dS(k) from VMC and DMC h5 files."""
  if 'dseries' in kwargs:
    dseries = kwargs.pop('dseries')
  else:
    dseries = 3
  use_mixed = kwargs.pop('use_mixed', False)
  uk, uskm1, uske1 = get_dsk1d(norb, dseries, stat_dir, **kwargs)
  if use_mixed:
    return uk, uskm1, uske1
  uk, uskm0, uske0 = get_dsk1d(norb, 0, stat_dir, **kwargs)
  return uk, 2*uskm1-uskm0, (4*uske1**2+uske0**2)**0.5
