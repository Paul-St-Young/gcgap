import numpy as np

# =============== begin convenience functions ===============
def get_axes(name, natom):
  """Get the diamond simulation cell for 'c' or 'si' at ambient conditions.

  Args:
    name (str): 'si' or 'c'
    natom (int): number of atoms, must be compatible with either the primitive
     cell or one of the conventional supercells (2, 8, 64, 216, 521, etc.).
  Return:
    np.array: simulation cell axes
  """
  bohr = 0.5291772  # angstrom
  if name == 'si':
    axes0, elem0, pos0 = diamond_silicon_prim()
  elif name == 'c':
    axes0, elem0, pos0 = diamond_carbon_prim()
  # primitive cell
  axes0 /= bohr
  # conventional cell
  tmat0 = np.ones(3)-2*np.eye(3)
  axes1 = np.dot(tmat0, axes0)
  # supercells
  if natom == 2:
    return axes0
  elif natom == 8:
    return axes1
  else:
    nx = (natom/8.)**(1./3)
    if not np.isclose(int(round(nx)), nx):
      msg = 'cannot get %s N=%d conventional cell' % (name, natom)
      raise RuntimeError(msg)
    return nx*axes1

def get_cubic_tmat(natom):
  nx = (natom/8.)**(1./3)
  if not np.isclose(int(round(nx)), nx):
    msg = 'cannot get N=%d conventional cell' % natom
    raise RuntimeError(msg)
  tmat = nx*(np.ones(3)-2*np.eye(3))
  return tmat

def get_axes_elem_pos(name, natom):
  bohr = 0.5291772  # angstrom
  if name == 'si':
    axes0, elem0, pos0 = diamond_silicon_prim()
  elif name == 'c':
    axes0, elem0, pos0 = diamond_carbon_prim()
  # primitive cell
  axes0 /= bohr
  # get tiling matrix
  tmat = get_cubic_twists(natom)

def get_cubic_twists(tgrid0):
  """Get the symmetry-inequivalent twists in a cubic cell.
  The grid will contain the Gamma point.

  hint: print np.array2string(x, precision=16, separator=',')

  Args:
    tgrid0 (int): number of twist in each dimension
  Return:
    np.array: twists in fractional units
  """
  if tgrid0 == 2:
    qvecs = np.array([
      [ 0.0,  0.0,  0.0],
      [ 0.0,  0.0, -0.5],
      [ 0.0, -0.5, -0.5],
      [-0.5, -0.5, -0.5]
    ])
    wts = np.array([0.25, 0.75, 0.75, 0.25])
  elif tgrid0 == 3:
    qvecs = np.array([
      [0.0, 0.0, 0.0],
      [0.0, 0.0, 1./3],
      [0.0, 1./3, 1./3],
      [1./3, 1./3, 1./3]
    ])
    wts = np.array([0.0740741, 0.4444444, 0.8888889, 0.5925926])
  elif tgrid0 == 4:
    qvecs = np.array([
      [0.0 , 0.0 , 0.0],
      [0.0 , 0.0 , 0.25],
      [0.0 , 0.0 ,-0.5],
      [0.0 , 0.25, 0.25],
      [0.0 , 0.25,-0.5],
      [0.0 , -0.5,-0.5],
      [0.25, 0.25, 0.25],
      [0.25, 0.25,-0.5],
      [0.25, -0.5,-0.5],
      [-0.5, -0.5,-0.5]
    ])
    wts = np.array([0.03125, 0.1875, 0.09375, 0.375, 0.375, 0.09375, 0.25, 0.375, 0.1875, 0.03125])
  elif tgrid0 == 5:
    qvecs = np.array([
      [0.0, 0.0, 0.0],
      [0.0, 0.0, 0.2],
      [0.0, 0.0, 0.4],
      [0.0, 0.2, 0.2],
      [0.0, 0.2, 0.4],
      [0.0, 0.4, 0.4],
      [0.2, 0.2, 0.2],
      [0.2, 0.2, 0.4],
      [0.2, 0.4, 0.4],
      [0.4, 0.4, 0.4]
    ])
    wts = np.array([0.016, 0.096, 0.096, 0.192, 0.384, 0.192, 0.128, 0.384, 0.384, 0.128])
  elif tgrid0 == 8:
    qvecs = np.array([
      [ 0.   , 0.   , 0.   ],
      [ 0.   , 0.   , 0.125],
      [ 0.   , 0.   , 0.25 ],
      [ 0.   , 0.   , 0.375],
      [ 0.   , 0.   ,-0.5  ],
      [ 0.   , 0.125, 0.125],
      [ 0.   , 0.125, 0.25 ],
      [ 0.   , 0.125, 0.375],
      [ 0.   , 0.125,-0.5  ],
      [ 0.   , 0.25 , 0.25 ],
      [ 0.   , 0.25 , 0.375],
      [ 0.   , 0.25 ,-0.5  ],
      [ 0.   , 0.375, 0.375],
      [ 0.   , 0.375,-0.5  ],
      [ 0.   ,-0.5  ,-0.5  ],
      [ 0.125, 0.125, 0.125],
      [ 0.125, 0.125, 0.25 ],
      [ 0.125, 0.125, 0.375],
      [ 0.125, 0.125,-0.5  ],
      [ 0.125, 0.25 , 0.25 ],
      [ 0.125, 0.25 , 0.375],
      [ 0.125, 0.25 ,-0.5  ],
      [ 0.125, 0.375, 0.375],
      [ 0.125, 0.375,-0.5  ],
      [ 0.125,-0.5  ,-0.5  ],
      [ 0.25 , 0.25 , 0.25 ],
      [ 0.25 , 0.25 , 0.375],
      [ 0.25 , 0.25 ,-0.5  ],
      [ 0.25 , 0.375, 0.375],
      [ 0.25 , 0.375,-0.5  ],
      [ 0.25 ,-0.5  ,-0.5  ],
      [ 0.375, 0.375, 0.375],
      [ 0.375, 0.375,-0.5  ],
      [ 0.375,-0.5  ,-0.5  ],
      [-0.5  ,-0.5  ,-0.5  ]
    ])
    wts = np.array([
      0.0039062,0.0234375,0.0234375,0.0234375,0.0117188,0.046875 ,0.09375  ,
      0.09375  ,0.046875 ,0.046875 ,0.09375  ,0.046875 ,0.046875 ,0.046875 ,
      0.0117188,0.03125  ,0.09375  ,0.09375  ,0.046875 ,0.09375  ,0.1875   ,
      0.09375  ,0.09375  ,0.09375  ,0.0234375,0.03125  ,0.09375  ,0.046875 ,
      0.09375  ,0.09375  ,0.0234375,0.03125  ,0.046875 ,0.0234375,0.0039062
    ])
  return qvecs, wts

def cubic_twist_grid(s1, tgrid0, nk=8, ndig=6):
  """Edit structure and its folded_structure to
   target a cubic grid of twists for the supercell.

  Args:
    s1 (nexus.structure): supercell with its folded primitive cell
    tgrid0 (int): number of twists along each dimension
  """
  from gcgap.dft_band import get_cubic_twists
  from qharv.inspect import axes_pos
  qvecs, qwts = get_cubic_twists(tgrid0)
  fs1 = s1.folded_structure
  axes = s1.axes
  axes0 = fs1.axes
  volfac = axes_pos.volume(axes)/axes_pos.volume(axes0)
  raxes = axes_pos.raxes(axes)
  tvecs = np.dot(qvecs, raxes)
  nk1 = len(tvecs)
  s1.kpoints = tvecs
  s1.kweights = np.ones(nk1)/float(nk1)
  # create folded twists
  raxes0 = axes_pos.raxes(axes0)
  #  first create supercell reciprocal lattice
  ukpos = axes_pos.cubic_pos(nk)-nk//2
  kpos = np.dot(ukpos, raxes)
  kposl = []
  for tvec in tvecs:
    # then fold into primitive cell BZ
    kpos1 = axes_pos.pos_in_axes(raxes0, kpos+tvec)
    kpos2 = np.unique(kpos1.round(ndig), axis=0)
    # check the number of twists
    nktile = len(kpos2)
    if not np.isclose(nktile, volfac):
      msg = 'tiling %d kpoints for %.1f volume' % (nktile, volfac)
      msg += 'consider increase nk'
      raise RuntimeError(msg)
    kposl.append(kpos2)
  kvecs = np.concatenate(kposl, axis=0)
  fs1.kpoints = kvecs
  nk2 = len(kvecs)
  fs1.kweights = np.ones(nk2)/float(nk2)
# =============== convenience functions end ===============

def diamond_carbon_prim(alat=3.567/2.):
  """Get primitive cell of diamond carbon structure.
  !!!! alat is in units of angstrom
  default is ~6.74/2=3.37 bohr
  C-C separation is 1.54456 angstrom

  Args:
    alat (float, optional): lattice constant in angstrom, default = 3.567/2
  Return:
    (np.array, np.array, np.array): (axes0, elem0, upos0), which are
      (lattice vectors, atomic symbols, reduced atomic coordinates)
  Example:
    axes0, elem0, upos0 = diamond_carbon_prim()
  """
  axes0 = alat*(np.ones(3)-np.eye(3))  # FCC primitive cell
  upos0 = np.array([
    [0.50, 0.50, -0.50],
    [-0.25, 0.75,  0.75]
  ])
  elem0 = np.array(['C']*len(upos0))
  return axes0, elem0, upos0

def fcc_kxy_bandpath(axes0, npoints=64, sp_names=None):
  """Get reciprocal-space path for FCC crystal band structure in kx-ky plane.

  Args:
    axes0 (np.array): FCC lattice vectors, used to decide kpath length
    npoints (int, optional): number of points on kpath, default is 64
    sp_names (list, optional): special kpoint names as a list of strings,
      default = ['X', 'Gamma', 'X', 'W', 'K', 'Gamma']; e.g. ['Gamma', 'X']
  Return:
    (np.array,)*4: (ukpts, kpath, sp_points, sp_names), which are
      (kpoints in reduced units, 1D coordinate of each kpoint on the kpath,
       1D coordinates of special kpoints, names of special kpoints)
  """
  from ase.dft.kpoints import ibz_points, get_bandpath
  kpoints = ibz_points['fcc']
  if sp_names is None:
    sp_names = ['X', 'Gamma', 'X', 'W', 'K', 'Gamma']
  path = [kpoints[name] for name in sp_names]
  kpts_reduced, kpath, sp_points = get_bandpath(path, axes0, npoints=npoints)
  return kpts_reduced, kpath, sp_points, sp_names

def diamond_silicon_prim(alat=2.715265):
  axes0 = alat*(np.ones(3)-np.eye(3))
  upos0 = np.array([
    [0.50, 0.50, -0.50],
    [-0.25, 0.75,  0.75]
  ])
  elem0 = np.array(['Si']*len(upos0))
  return axes0, elem0, upos0

def get_kbraggs(axes, nx=10, nb=1, tol=1e-3):
  from qharv.inspect import axes_pos
  # step 1: get a few shells of the reciprocal lattice
  raxes = axes_pos.raxes(axes)
  upos = axes_pos.cubic_pos(nx)-nx//2
  kvecs = np.dot(upos, raxes)
  # step 2: find the shortest G vectors
  kmags = np.linalg.norm(kvecs, axis=-1)
  unique_kmags = np.unique(kmags.round(4))
  unique_kmags.sort()
  # step 3: return the first nb shells of Bragg peaks
  bragg_mags = unique_kmags[1:nb+1]  # skip k=0
  kbl = []
  for kbmag in bragg_mags:
    sel = abs(kmags-kbmag)<tol
    kbl.append(kvecs[sel])
  kbraggs = np.concatenate(kbl, axis=0)
  return kbraggs

def csi_kbraggs(name, nb=1):
  from gcgap.dft_band import get_kbraggs
  if name == 'si':
    from gcgap.dft_band import diamond_silicon_prim
    axes0, elem0, pos0 = diamond_silicon_prim()
  elif name == 'c':
    from gcgap.dft_band import diamond_carbon_prim
    axes0, elem0, pos0 = diamond_carbon_prim()
  bohr = 0.5291772
  axes0 /= bohr
  return get_kbraggs(axes0, nb=nb)

def get_ukbrags(kbrags, fwf):
  from qharv.seed import wf_h5
  from qharv.inspect import axes_pos
  fp = wf_h5.read(fwf)
  axes = wf_h5.get(fp, 'axes')
  fp.close()
  raxes = axes_pos.raxes(axes)
  ukcands = np.dot(kbrags, np.linalg.inv(raxes))
  ukbrags = np.round(ukcands).astype(int)
  assert np.allclose(ukcands, ukbrags)
  return ukbrags
