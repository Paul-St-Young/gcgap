import numpy as np

def get_atb(scf_out, **kwargs):
  import qe_reader as qer
  # get DFT bands
  data1 = qer.parse_nscf_bands(scf_out, **kwargs)
  mat1 = data1['bands']
  bands = mat1.T  # transpose to set twist as fastest index
  norb, ntwist = bands.shape

  # get twist vectors
  tvecs = data1['kvecs']

  # get axes
  from qharv.reel import ascii_out
  mm = ascii_out.read(scf_out)
  axes = get_axes(mm)

  return axes, tvecs, bands

def get_somat(mymu, bands):
  norb, ntwist = bands.shape
  nmu = len(mymu)
  somat = np.zeros([nmu, norb, ntwist], dtype=bool)
  for imu, mu in enumerate(mymu):
    somat[imu] = bands <= mu  # occupy all SPOs below mu
  return somat

def get_nelecs(mydf):
  nelecs = mydf.nelec.unique()
  nelecs.sort()
  assert len(nelecs) == 3
  return nelecs

def get_murmua(mydf, yname, ymult):
  nelecs = get_nelecs(mydf)
  eml = []
  for nelec in nelecs:
    sel = mydf.nelec == nelec
    e1 = mydf.loc[sel, yname].squeeze()
    eml.append(e1)
  mur = (eml[1]-eml[0])*ymult/(nelecs[1]-nelecs[0])
  mua = (eml[2]-eml[1])*ymult/(nelecs[2]-nelecs[1])
  return {'mur': mur, 'mua': mua}

def get_axes(mm, ndim=3):
  from qharv.reel import ascii_out
  idx = mm.find('crystal axes')
  mm.seek(idx)
  mm.readline()
  rows = []
  for idim in range(ndim):
    line = mm.readline()
    text0 = line.split('=')[-1]
    text = ascii_out.lr_mark(text0, '(', ')')
    row = map(float, text.split())
    rows.append(row)
  axes = np.array(rows, dtype=float)
  return axes

def dosen(mu, dosx, dosy):
  """Calculate energy E and number of electrons N from density of states (DOS).

  Args:
    mu (float): chemical potential
    dosx (np.array): energy
    dosy (np.array): density of states at energy
  Return:
    (float, float): (energy, number)
  """
  sel = dosx <= mu
  em = np.trapz(dosy[sel]*dosx[sel], dosx[sel])
  nm = np.trapz(dosy[sel], dosx[sel])
  return em, nm

def gcen(mu, bands, wmat):
  osel = bands <= mu
  em = np.sum(bands[osel]*wmat[osel])
  nm = np.sum(wmat[osel])
  return em, nm

def unique_nem(nm, em, mymu):
  from static_correlation import kshell_sels
  sels = kshell_sels(nm, 1000)
  nsh = len(sels)
  uem = np.zeros(nsh)
  unm = np.zeros(nsh)
  umu = np.zeros(nsh)
  for ish, sel in enumerate(sels):
    uem[ish] = em[sel].mean()
    unm[ish] = nm[sel].mean()
    umu[ish] = mymu[sel].mean()
  return unm, uem, umu

def get_gcen(mymu, mu0, scf_out, iv=None, wtol=1e-10):
  """Calculate energy E and number of electrons N from DFT band.

  Args:
    mymu (np.array): array of chemical potentials to use
    mu0 (float): one chemical potential inside the gap (e.g. Fermi energy)
    scf_out (str): quantum espresso output with band info (verbosity=.high.)
    iv (int, optional): index of the top valence band
      If given, then use two bands.
      Namely, consider only top of valence and bottom of conduction bands.
    wtol (float, optional): tolerance on total weight deviation
  Return:
    (np.array, np.array): (uem, unm, umu), which are unique
    (GCTA energy mean, GCTA number of electrons, chemical potential)
  Example:
    >>> mu0 = 8  # eV
    >>> mymu = np.linspace(mu0-5, mu0+5, 32)
    >>> em, nm = get_gcen(mymu, mu0, 'scf.out')
  """
  from qharv.plantation.sugar import concat_return
  # read kgrid weights
  import qe_reader as qer
  try:
    remove_copy = True
    weights = qer.get_weights(scf_out, remove_copy, atol=wtol)
  except RuntimeError:
    remove_copy = False
    weights = qer.get_weights(scf_out, remove_copy, atol=wtol)
  # read kgrid
  axes, tvecs, bands = get_atb(scf_out)
  if iv is not None:
    bands = bands[iv:iv+2, :]
  wmat = np.array([weights for ib in range(bands.shape[0])])
  # get reference (E0, N0)
  e0, n0 = gcen(mu0, bands, wmat)
  # get relative (E, N)
  ret = concat_return(False)(gcen)(mymu, bands, wmat)
  em, nm = np.array(ret).T
  # shell average
  unm, uem, umu = unique_nem(nm, em, mymu)
  return uem-e0, unm-n0, umu

class EnergyNumberFitQBA:
  """Fit <E>(mu) <N>(mu) to extract band gap."""
  def __init__(self, mymu, em, nm):
    """Store chemical potentials and corresponding energies and numbers.

    After storing input, initialize internal fitting forms based on the
     quadratic-band approximation (QBA).

    Args:
      mymu (np.array): chemical potentials.
      em (np.array): mean of GCTA energies.
      nm (np.array): mean of GCTA quasiparticle numbers.
    """
    self.mymu = mymu  # useful for choosing fit range
    self.em = em
    self.nm = nm
    self.en_fit_fmt = r'%3.2f N+%3.2e N$^{5/3}$'
    self.dedn_fit_fmt = r'%3.2f+%3.2e N$^{2/3}$'
  def get_dedn(self):
    """Calculate d<E>/d<N> using finite difference.

    Return:
      (np.array, np.array): (nm, dem/dnm)
    """
    x = 0.5*(self.nm[1:]+self.nm[:-1])
    y = np.diff(self.em)/np.diff(self.nm)
    return x, y
  @staticmethod
  def en_model(n, a, b):
    return a*n+b*abs(n)**(5./3)
  @staticmethod
  def linear(x, a, b):
    return a+b*x
  @staticmethod
  def fit_sel(sel, x, y, model):
    """Fit a selection of (x, y) to given model.

    Args:
      sel (np.array): boolean selector array
      x (np.array): x values
      y (np.array): y values
      model (callable): fitting function
    Return:
      (tuple, tuple): (popt, perr), which are
       (fitted parameter values, fitted parameter error bars)
    """
    from scipy.optimize import curve_fit
    popt, pcov = curve_fit(model, x[sel], y[sel])
    perr = np.sqrt(np.diag(pcov))
    return popt, perr
  def fit_left_right(self, lsel, rsel, x, y, model,
                     ax=None, label_fmt=None):
    # perform fit
    lpopt, lperr = self.fit_sel(lsel, x, y, model)
    rpopt, rperr = self.fit_sel(rsel, x, y, model)
    # maybe plot fit
    if ax is not None:
      # hard code plotting style
      nx = 64  # number of grid points to show fit
      sel_styles = {'ls': '', 'marker': 'o', 'fillstyle': 'none'}
      # show left
      line = ax.plot(x[lsel], y[lsel], **sel_styles)
      finex = np.linspace(min(x[lsel]), max(x[lsel]), nx)
      if label_fmt is None:
        label = ''
      else:
        label = label_fmt % tuple(lpopt)
      ax.plot(finex, model(finex, *lpopt), c=line[0].get_color(),
              label=label)
      # show right
      line = ax.plot(x[rsel], y[rsel], **sel_styles)
      finex = np.linspace(min(x[rsel]), max(x[rsel]), nx)
      if label_fmt is None:
        label = ''
      else:
        label = label_fmt % tuple(rpopt)
      ax.plot(finex, model(finex, *rpopt), c=line[0].get_color(),
              label=label)
    return lpopt, lperr, rpopt, rperr
  def fit_en(self, mu0, mufit_min, mufit_max, ax=None, model=None):
    """Fit <E> vs. <N> within selected chemical potential range.

    Args:
      mu0 (float): some chemical potential inside the gap region
      mufit_min (float): lowest chemical potential used in fit
      mufit_max (float): highest chemical potential used in fit
      ax (plt.Axes, optional): axes to show fits, default None
    Return:
      (tuple,)*4: (lpopt, lperr, rpopt, rperr), which are parameter
       values and errors left (mu<mu0) of gap and right of the gap.
    """
    if model is None:
      model = self.en_model
    x = self.nm
    y = self.em
    label_fmt = self.en_fit_fmt
    lsel = (mufit_min < self.mymu) & (self.mymu <= mu0)
    rsel = (mu0 <= self.mymu) & (self.mymu < mufit_max)
    data = self.fit_left_right(lsel, rsel, x, y, model,
                               ax=ax, label_fmt=label_fmt)
    lpopt, lperr, rpopt, rperr = data
    return lpopt, lperr, rpopt, rperr
  def fit_dedn(self, mu0, mufit_min, mufit_max, ax=None, model=None):
    if model is None:
      model = self.linear
    x0, y0 = self.get_dedn()
    x = abs(x0)**(2./3)
    y = y0
    mymu = 0.5*(self.mymu[1:]+self.mymu[:-1])
    label_fmt = self.dedn_fit_fmt
    sel0 = ~np.isnan(y0)
    lsel = (mufit_min < mymu) & (mymu <= mu0) & sel0
    rsel = (mu0 <= mymu) & (mymu < mufit_max) & sel0
    data = self.fit_left_right(lsel, rsel, x, y, model,
                               ax=ax, label_fmt=label_fmt)
    lpopt, lperr, rpopt, rperr = data
    return lpopt, lperr, rpopt, rperr
